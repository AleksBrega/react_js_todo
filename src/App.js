import React, {useEffect ,useState} from "react";
import TodoForm from "./Components/todo_form";
import TodoList from "./Components/todo_list";
import './App.css';

const LOCAL_STORAGE = "todo_list_storage";

function App() {
    const [todos, setTodos] = useState([])

    useEffect(() => {
        const storageTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE));
        if (storageTodos) {
            setTodos(storageTodos);
        }
    }, []);

    useEffect(() => {
        localStorage.setItem(LOCAL_STORAGE, JSON.stringify(todos));
    }, [todos]);

    function add_todo(todo) {
        setTodos([todo, ...todos]);
    }

    function toggle_complete(id) {
        setTodos(
            todos.map(todo => {
                if (todo.id === id) {
                    return {
                        ...todo, completed: !todo.completed
                    }
                }
                return todo
            })
        );
    }

    function remove_todo(id){
        setTodos(todos.filter(todo => todo.id !== id));

    }

  return (
    <div className="App">
      <header className="App-header">
          <p>React to do</p>
          <TodoForm add_todo={add_todo}/>
          <TodoList todos={todos} toggle_copmlete={toggle_complete} remove_todo={remove_todo} />
      </header>
    </div>
  );
}

export default App;
