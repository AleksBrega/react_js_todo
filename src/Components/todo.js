import React from "react";

function Todo({todo, toggle_complete, remove_todo}) {

    function handle_remove() {
        remove_todo(todo.id);
    }

    function handle_checkbox() {
        toggle_complete(todo.id);
    }

    return (
        <div style={{ display: "flex"}}>
            <input type="checkbox" onClick={handle_checkbox} />
            <li
                style={{
                    color: "white",
                    textDecoration: todo.completed ? "line-through" : null
                }}
            >{todo.task}</li>
            <button onClick={handle_remove}>X</button>
        </div>
    )
}
export default Todo