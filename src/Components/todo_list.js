import React from "react";
import Todo from "./todo"

function TodoList({ todos, toggle_copmlete, remove_todo }) {
    return (
        <ul>
            {todos.map(todo => (
                <Todo key={todo.id}
                      todo={todo}
                      toggle_complete={toggle_copmlete}
                      remove_todo={remove_todo}
                />
            ))}
        </ul>
    )
}

export default TodoList;