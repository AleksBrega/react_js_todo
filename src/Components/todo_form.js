import React, {useState} from "react";
import {v4 as uuid} from "uuid";

function TodoForm({add_todo}) {
    const [todo, SetTodo] = useState({
        id: "",
        task: "",
        completed: false,
    });

    function handle_task_input_change(e) {
        SetTodo({ ...todo, task: e.target.value });
    }

    function handle_submit(e) {
        e.preventDefault();
        if (todo.task.trim()) {
            add_todo({ ...todo, id: uuid() });
            SetTodo({...todo, task: "" });
        }
    }

    return (
        <form onSubmit={handle_submit}>
            <input
                name="task"
                type="text"
                value={todo.task}
                onChange={handle_task_input_change}
            />
            <button type="submit">Submit</button>
        </form>
    );
}

export default TodoForm;